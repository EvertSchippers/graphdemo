﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Fugro.G2O;

namespace SegmentFitting
{
    public class LineSegment
    {
        public Point A;
        public Point B;

        public LineSegment(Point a, Point b)
        {
            A = a;
            B = b;

            var tangent = b - a;
            var normal = new Vector(-tangent.Y, tangent.X);
            normal.Normalize();
            

        }

        public Point ProjectPointOnLine(Point P)
        {
            Vector AP = P - A;
            Vector AB = B - A;

            double a = Dot(AP, AB);
            double b = Dot(AB, AB);
            Vector C = (a / b) * AB;

            return A + C;
        }

        public static double Dot(Vector p1, Vector p2)
        {
            return p1.X * p2.X + p1.Y * p2.Y ;
        }
        
        public double GetSignedDistance(Point point)
        {
            var projection = ProjectPointOnLine(point);
            return (projection - point).Length;

            //var vector = B - A;

            //var distance = (point.X * vector.Y - point.Y * vector.X + B.X * A.Y - B.Y * A.X)
            //               / vector.Length;

            //return distance;
        }
    }

    public class PoseState : State<Transform>
    {
        public PoseState(Transform initial)
            : base(initial, 3)
        {
        }

        protected override Transform OnUpdate(double[] delta)
        {
            var currentEstimate = Estimate;

            var rotation = new RotateTransform(delta[0]);
            var translation = new TranslateTransform(delta[1], delta[2]);

            var result = new TransformGroup();
            result.Children.Add(rotation);
            result.Children.Add(translation);
            result.Children.Add(currentEstimate);

            return new MatrixTransform(result.Value);
        }
    }

    public class PointObservation : RobustifiedObservation
    {
        private readonly Point m_PointOnRailHead;
        private readonly State<Transform> m_RailHeadPose;
        private readonly IReadOnlyList<LineSegment> m_RailHead;

        public PointObservation(Point pointOnRailHead, State<Transform> railHeadPose, IReadOnlyList<LineSegment> railHead)
            : base(new []{ 1.0 }, railHeadPose)
        {
            m_PointOnRailHead = pointOnRailHead;
            m_RailHeadPose = railHeadPose;
            m_RailHead = railHead;
        }

        public double[] ComputeErrors()
        {
            double error = double.PositiveInfinity;

            foreach (var lineSegment in m_RailHead)
            {
                var transformedA = m_RailHeadPose.Estimate.Transform(lineSegment.A);
                var transformedB = m_RailHeadPose.Estimate.Transform(lineSegment.B);

                var thisError = new LineSegment(transformedA, transformedB).GetSignedDistance(m_PointOnRailHead);

                if (Math.Abs(error) >= Math.Abs(thisError))
                {
                    error = thisError;
                }
            }

            return new[] { error };
        }

        protected override double[] OnComputeError()
        {
            return ComputeErrors();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var dataFile = Path.Combine(Directory.GetCurrentDirectory(), @"..\..\Data\scene.csv");
            var resultFile = Path.Combine(Directory.GetCurrentDirectory(), @"..\..\Data\solution.csv");

            var a = new LineSegment(new Point(0, 0), new Point(0, 100));
            var b = new LineSegment(new Point(0, 100), new Point(20, 80));
            var c = new LineSegment(new Point(20, 80), new Point(20, 0));

            var railHead = new[] { a, b, c };

            TransformGroup initialValue = new TransformGroup();
            initialValue.Children.Add(new RotateTransform(90));
            initialValue.Children.Add(new TranslateTransform(0, 0));

            var poseState = new PoseState(initialValue);

            var dataPoints = File.ReadAllLines(dataFile)
                                 .Select(line => line.Split(','))
                                 .Select(s => new Point(double.Parse(s[0]), double.Parse(s[1])))
                                 .ToList();

            var observations = dataPoints.Select(p => new PointObservation(p, poseState, railHead)).ToList();

            var graph = new Graph();

            graph.AddObservation(observations);

            var inlierFraction = 0.05;

            graph.IterationStarting += (aa, bb) =>
            {
                var delta = observations.Select(o => o.ComputeErrors()[0])
                                        .Select(e => e*e)
                                        .OrderBy(e => e).Take((int) (inlierFraction * observations.Count)).Last();

                foreach (var pointObservation in observations)
                {
                    pointObservation.Delta = 50 * delta;
                }               
            };
            
            graph.Optimize();

            List<Point> resultPoints = new List<Point>();

            foreach (var lineSegment in railHead)
            {
                var transformedA = poseState.Estimate.Transform(lineSegment.A);
                var transformedB = poseState.Estimate.Transform(lineSegment.B);
                var transformedLine = new LineSegment(transformedA, transformedB);

                var pointsOnSegment = Enumerable.Range(0, 100)
                                                .Select(i => transformedLine.A + (i / 100.0) * (transformedLine.B - transformedLine.A));

                resultPoints.AddRange(pointsOnSegment);
                
            }

            File.WriteAllLines(resultFile, resultPoints.Select(r => $"{r.X},{r.Y}"));


        }
    }
}
