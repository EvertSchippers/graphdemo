﻿using System;
using Fugro.G2O;

namespace SegmentFitting
{
    public abstract class RobustifiedObservation : Observation
    {
        protected RobustifiedObservation(double[] stdevs, params BaseState[] states)
            : base(stdevs, states)
        {
            Delta = double.MaxValue;
        }

        protected RobustifiedObservation(int dimension, double[] precisionMatrix, params BaseState[] states)
            : base(dimension, precisionMatrix, states)
        {
            Delta = double.MaxValue;
        }

        public double Delta
        {
            get;
            set;
        }

        protected override void OnRobustify(double squaredError, out double scaledError, out double firstDerivative)
        {
            scaledError = HuberFromTorrSquared(squaredError);
            firstDerivative = HuberFromTorrSquaredDerivative(squaredError);
        }

        private double HuberFromTorrSquared(double e2)
        {
            if (e2 <= Delta)
            {
                return e2;
            }

            if (e2 > 9 * Delta)
            {
                return 4 * Delta;
            }

            double threshold = Math.Sqrt(Delta);
            double error = Math.Sqrt(e2);

            var e = (6 * error - e2 / threshold - threshold) / 4;

            return e * e;
        }

        private double HuberFromTorrSquaredDerivative(double e2)
        {
            if (e2 <= Delta)
            {
                return 1.0;
            }

            if (e2 > 9 * Delta)
            {
                return 0.0;
            }

            return Delta / e2;
            // This is actually not the derivative of the HuberFromTorrSquared, but results in lower reprojection errors in SeaStriper calibration.
            // The proper formula is:
            // return Math.Sqrt(HuberFromTorrSquared(e2)) * (3 / Math.Sqrt(e2) - Math.Sqrt(Delta)) / 2;
        }
    }
}